﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MarketPlaceService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICareStoreService" in both code and config file together.
    [ServiceContract]
    public interface ICareStoreService
    {
        [OperationContract]
        void DoWork();
    }
}
