﻿using CareStoreServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CareStoreMarketplaceServiceWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class CareStoreMarketplaceWcfService : ICareStoreMarketplaceContract
    {
        private const string UserFileDir = "/Files/Users/";
        private const string UserFilePath = UserFileDir +"ulist.dat";
        private readonly string _fingerTemplateDir = "/Files/Users/FingerTemplates/";
        private readonly string _facialTemplateDir = "/Files/Users/FacialTemplates/";
        private readonly string _profileImageDir = "/Files/Users/ProfileImages/";

        /// <summary>
        /// Looks up link to driver file for profile
        /// </summary>
        /// <param name="profile">the name of the profile</param>
        /// <returns>link to driver</returns>
        /// <exception cref="ArgumentException">thrown if an unknown profile is provided</exception>
        public Uri GetDriverLink(string profile)
        {
            var link = new UriBuilder { Host = "carestoremarketplace.cloudapp.net" };
            switch (profile.ToLower())
            {
                case "pi":
                    link.Path = "/Files/Drivers/PiDriver.zip";
                    break;
                case "random":
                    link.Path = "/Files/Drivers/RandomDriver.zip";
                    break;
                default:
                    return null;
            }
            return link.Uri;
        }

        public IList<string> GetAppLinks()
        {
            var host = ConfigurationManager.AppSettings.Get("host");
            var port = int.Parse(ConfigurationManager.AppSettings.Get("port"));
            var username = ConfigurationManager.AppSettings.Get("username");
            var password = ConfigurationManager.AppSettings.Get("password");
            var remoteDirectory = ConfigurationManager.AppSettings.Get("remoteDirectory") + "/apps/";

            return SFTPFileLister.GetFileNames(host, port, username, password, remoteDirectory);
        }

        public void UpdateUser(UserProfileDataObject profile, byte[] profilePicture = null, byte[] facialTemplate = null, byte[] fingerTemplate = null)
        {
            List<UserProfileDataObject> users;

            try
            {
                users = File.Exists(UserFilePath) ? ReadFromFile() : new List<UserProfileDataObject>();
            }
            catch (Exception e)
            {

                throw e;
            }

            var indexOfItem = users.FindIndex(c => c.Id == profile.Id);
            if (indexOfItem != -1)
                users[indexOfItem] = profile;
            else
                users.Add(profile);

            if (profilePicture != null)
                StorePicture(profile.Id, profilePicture);

            if (facialTemplate != null)
                StoreFacialTemplate(profile.Id, facialTemplate);

            if (fingerTemplate != null)
                StoreFingerTemplate(profile.Id, fingerTemplate);


            WritetoFile(users);

        }

        public void UpdateFacialTemplate(string id, byte[] template)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            StoreFingerTemplate(id, template);
        }

        public void UpdateFingerTemplate(string id, byte[] template)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            StoreFingerTemplate(id, template);
        }

        public void UpdateProfilePicture(string id, byte[] profilePicture)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            StorePicture(id, profilePicture);
        }

        public List<UserProfileDataObject> GetUsers()
        {
            try
            {
                return ReadFromFile();
            }
            catch 
            {
                return null;
            }
            
        }

        public byte[] GetProfilePicture(string id)
        {
            var path = Path.Combine(_profileImageDir, id + ".dat");

            if (File.Exists(path))
                return File.ReadAllBytes(path);

            throw new ArgumentException("not a valid Id, no profilepicture exsists");
        }

        public byte[] GetFacialTemplate(string id)
        {
            var path = Path.Combine(_facialTemplateDir, id + ".dat");

            if (File.Exists(path))
                return File.ReadAllBytes(path);

            throw new ArgumentException("not a valid Id, no facialtemplate exsists");
        }

        public byte[] GetFingerTemplate(string id)
        {
            var path = Path.Combine(_fingerTemplateDir, id + ".dat");

            if (File.Exists(path))
                return File.ReadAllBytes(path);

            throw new ArgumentException("not a valid Id, no fingertemplate exsists");

        }

        public void RemoveUser(string id)
        {
            List<UserProfileDataObject> uList = null;

            try
            {
                uList = ReadFromFile();

                var user = uList.Single(c => c.Id == id);

                uList.Remove(user);

                WritetoFile(uList);

                if (user.HasFacialProfileTemplate)
                {
                    var path = Path.Combine(_facialTemplateDir, id + ".dat");
                    File.Delete(path);
                }
                if (user.HasFingerPrintTemplate)
                {
                    var path = Path.Combine(_fingerTemplateDir, id + ".dat");
                    File.Delete(path);
                }
                if (user.HasProfileImage)
                {
                    var path = Path.Combine(_profileImageDir, id + ".dat");
                    File.Delete(path);
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void StoreFingerTemplate(string id, byte[] template)
        {
            var path = Path.Combine(_fingerTemplateDir, id + ".dat");

            if (!Directory.Exists(_fingerTemplateDir))
                Directory.CreateDirectory(_fingerTemplateDir);

            File.WriteAllBytes(path, template);
        }

        private void StoreFacialTemplate(string id, byte[] template)
        {
            var path = Path.Combine(_facialTemplateDir, id + ".dat");

            if (!Directory.Exists(_facialTemplateDir))
                Directory.CreateDirectory(_facialTemplateDir);

            File.WriteAllBytes(path, template);
        }

        private void StorePicture(string id, byte[] profilePicture)
        {
            var path = Path.Combine(_profileImageDir, id + ".dat");

            if (!Directory.Exists(_profileImageDir))
                Directory.CreateDirectory(_profileImageDir);

            File.WriteAllBytes(path, profilePicture);

        }

        private List<UserProfileDataObject> ReadFromFile()
        {
            try
            {
                using (Stream stream = File.Open(UserFilePath, FileMode.Open))
                {
                    var bin = new BinaryFormatter();

                    return (List<UserProfileDataObject>)bin.Deserialize(stream);
                }
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        private void WritetoFile(List<UserProfileDataObject> userList)
        {
            if (!Directory.Exists(UserFileDir))
                Directory.CreateDirectory(UserFileDir);

            try
            {
                using (Stream stream = File.Open(UserFilePath, FileMode.Create))
                {
                    var bin = new BinaryFormatter();
                    bin.Serialize(stream, userList);
                }
            }
            catch (IOException)
            {
            }

        }
    }
}
