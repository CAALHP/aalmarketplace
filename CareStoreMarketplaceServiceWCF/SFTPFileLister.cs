﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Renci.SshNet;

namespace CareStoreMarketplaceServiceWCF
{
    public class SFTPFileLister
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="remoteDirectory"></param>
        /// <returns></returns>
        public static List<string> GetFileNames(string host, int port, string username, string password, string remoteDirectory)
        {
            var list = new List<string>();
            var conInfo = new ConnectionInfo(host, port, username, new AuthenticationMethod[] { new PasswordAuthenticationMethod(username, password) });
            using (var sftp = new SftpClient(conInfo))
            {
                sftp.Connect();
                var files = sftp.ListDirectory(remoteDirectory);
                list.AddRange(from file in files where file.Name != "." && file.Name != ".." select file.Name);
            }
            return list;
        }
    }
}